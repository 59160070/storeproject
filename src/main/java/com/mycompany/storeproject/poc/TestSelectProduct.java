/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BenzNarathip
 */
public class TestSelectProduct {
    public static void main(String[]args){
        Connection conn = null;
        String dbPath = "./db/store.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite" + dbPath);
            System.out.println("Database Connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connection");
        }
        
        try {
            String sql = "SELECT Project_ID,Name,Price FROM Product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                System.out.println(id+" "+name+" "+price);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            if(conn!=null){
            conn.close();
            }
            
        } catch (SQLException ex) {
            System.out.println("Error: cannot close database");
        }
        
    }
}
